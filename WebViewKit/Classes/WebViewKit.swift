import UIKit
import WebKit
import SnapKit

public class WebViewController: BaseViewController {
    public var url: String = "index"
    public var isOnline: Bool = false
    var buttonLeft: UIBarButtonItem!
    var webview: WKWebView!
    var configuration: WKWebViewConfiguration!
    public var userContentController: WKUserContentController!
    var data: Dictionary<String, Any> = [
        "version": 1,
        "data": [
            "hello": "world",
            "hi": "nice to see you",
            "devices": ["iPhone", "iPad"]
        ]
    ]
}

// MARK: - Life Cycle
extension WebViewController {
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        title = "WebView"
        
        initWebView()
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


// MARK: - Views Initial
extension WebViewController {
    func initWebView() {
        configuration = WKWebViewConfiguration()
        configuration.userContentController = WKUserContentController()
        configuration.userContentController.add(self, name: "just")
        configuration.preferences = WKPreferences()
        configuration.preferences.javaScriptEnabled = true
        
        addScript("zepto.min")
        addScript("app")
        addScript("vconsole.min")
        
        webview = WKWebView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), configuration: configuration)
        webview.navigationDelegate = self
        webview.uiDelegate = self
        view.addSubview(webview)
        // layouts
        webview.snp.makeConstraints { (make) in
            make.top.equalTo(isX() ? 88 : 64)
            make.left.right.bottom.equalTo(0)
        }
        
        webview.open(url, isOnline: isOnline)
    }
}


// MARK: - Delegate
extension WebViewController: WKUIDelegate, WKScriptMessageHandler, WKNavigationDelegate {
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webview.title
        print("======")
        App.dictionaryToJSON(data) { string in
            print("JSONString: ", string)
            webview.evaluateJavaScript("javascript: App.emit('app.ready', '\(string)')") { (result, error) in
                print("result: ", result as Any)
                print("error: ", error as Any)
            }
        }
        print("======")
    }
    
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "just" {
            print("userContentController\(message.body as! Dictionary<String, String>)")
        }
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(WKNavigationActionPolicy.allow)
        
        let registeredSchemes = ["just", "http", "https"]
        let registeredMethods = ["to", "set"]

        if let url = navigationAction.request.url {
            if let scheme = url.scheme, let method = url.host, registeredSchemes.contains(scheme), registeredMethods.contains(method) {
                register(url.host ?? "", query: url.query ?? "")
            } else {
                print("scheme \(String(describing: url.scheme)) or method \(String(describing: url.host)) not registered")
            }
        } else {
            print("url is nil")
        }
    }
    
    public func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        completionHandler()
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - Callbacks
extension WebViewController {
    //
}

// MARK: - Tool Functions
extension WebViewController {
    func addScript(_ string: String) {
        do {
            let url = Bundle.main.url(forResource: string, withExtension: "js")
            if let url = url {
                let content = try String.init(contentsOf: url)
                let script = WKUserScript.init(source: content, injectionTime: .atDocumentStart, forMainFrameOnly: false)
                configuration.userContentController.addUserScript(script)
            } else {
                print("script's url is nil")
            }
        } catch let error {
            print("addScript error: ", error)
        }
    }
}
