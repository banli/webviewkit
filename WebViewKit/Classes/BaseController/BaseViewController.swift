//
//  BaseViewController.swift
//  Pods-WebViewKit_Example
//
//  Created by dabanli on 2019/10/31.
//

import UIKit
import SnapKit

public class BaseViewController: UIViewController {
    
    var navigationBar: UINavigationBar!
    var buttonBack: UIBarButtonItem!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        initNavBar()
    }
    
    //    override var prefersStatusBarHidden: Bool {
    //        return true
    //    }
    
    func initNavBar() {
        navigationBar = UINavigationBar()
        // hide 1px hairline
        navigationBar.clipsToBounds = true
        // set navigationBar transparent
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        view.addSubview(navigationBar)
        navigationBar.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(0)
            make.top.equalTo(isX() ? 44 : 20)
            make.height.equalTo(44)
        }
        
        buttonBack = UIBarButtonItem(title: "返回", style: .done, target: self, action: #selector(back))
        if let navigationController = navigationController {
            navigationItem.leftBarButtonItems = navigationController.viewControllers.count > 1 ? [buttonBack] : []
        }
        buttonBack.image = UIImage(named: "back")
        navigationBar.pushItem(navigationItem, animated: true)
    }
    
    @objc func back() {
        print("back")
        navigationController?.popViewController(animated: true)
    }
}

